import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';
import {DrugsPage} from './pages/pharmacist/drugs/drugs.page';

const routes: Routes = [
    {
        path: '', redirectTo: 'login', pathMatch: 'full'
    },
    {
        path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
    },
    {
        path: 'login',
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
    },
    {
        path: 'admin-dashboard',
        loadChildren: () => import('./pages/admin/admin-dashboard/admin-dashboard.module').then(m => m.AdminDashboardPageModule),
        // canActivate: [AuthGuard],
        // data: {
        //     role: 'admin'
        // }
    },
    {
        path: 'pharmacist-tabs',
        loadChildren: () => import('./pages/pharmacist/pharmacist-tabs/pharmacist-tabs.module').then(m => m.PharmacistTabsPageModule),
        canActivate: [AuthGuard],
        data: {
            role: 'pharmacist'
        }
    },
    {
        path: 'available-drugs/drugs/:drugId',
        loadChildren: () => import('./pages/pharmacist/available-drugs/available-drugs.module').then(m => m.AvailableDrugsPageModule)
    },
    {
        path: 'pharmacies/:pharmacyId',
        loadChildren: () => import('./pages/pharmacist/my-pharmacy/my-pharmacy.module').then(m => m.MyPharmacyPageModule)
    },
    {
        path: 'caretaker-tabs',
        loadChildren: () => import('./pages/caretaker/caretaker-tabs/caretaker-tabs.module').then(m => m.CaretakerTabsPageModule)
    },
  {
    path: 'treatments',
    loadChildren: () => import('./pages/caretaker/treatments/treatments.module').then( m => m.TreatmentsPageModule)
  },


    // {
    //     path: 'caretaker-tabs',
    //     loadChildren: () => import('./pages/pharmacist/pharmacist-tabs/pharmacist-tabs.module').then(m => m.PharmacistTabsPageModule),
    //     // canActivate: [AuthGuard],
    //     // data: {
    //     //     role: 'caretaker'
    //     // }
    // },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
