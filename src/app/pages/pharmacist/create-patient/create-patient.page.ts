import {Component, OnInit, QueryList} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {IonContent, LoadingController, ToastController} from '@ionic/angular';
import {PatientService} from '../../../services/patient.service';


@Component({
    selector: 'app-create-patient',
    templateUrl: './create-patient.page.html',
    styleUrls: ['./create-patient.page.scss'],
})

export class CreatePatientPage implements OnInit {
    createPatientForm: FormGroup;
    valid = true;
    private userContent: QueryList<any>;

    constructor(private patient: PatientService,
                private router: Router,
                public formBuilder: FormBuilder,
                public toastController: ToastController,
                public loadingController: LoadingController) {
    }

    ngOnInit() {
        this.createPatientForm = this.formBuilder.group({
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            weight: ['', [Validators.required]],
            height: ['', [Validators.required]],
            birthDate: [''],
            gender: ['', [Validators.required]],
            phone: ['', [Validators.required]],
            TIS: ['', [Validators.required]],
            city: ['', [Validators.required]],
            DNI: ['', [Validators.required]],
        });
    }

    errorControl() {
        return this.createPatientForm.controls;
    }

    async submitForm() {
        this.valid = this.createPatientForm.valid;

        if (this.valid) {
            this.patient.storePatient(this.createPatientForm.value).subscribe(async (response: any) => {
                if (response) {
                    await this.presentToast();
                    this.createPatientForm.reset();
                    const url = this.router.url.replace('create', response.id);
                    await this.router.navigate([url]);
                }
            });
        } else {
            // this.showError();
        }
    }

    // showError() {
    //     if (!this.valid) {
    //         setTimeout(() => {
    //             this.getContent().scrollToBottom(500);
    //         }, 300);
    //     }
    // }

    getContent() {
        return document.querySelector('#ioncontent');
        // if (element instanceof IonContent) {
        //     return element;
        // }
    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: 'The patient was successfully created.',
            duration: 2000
        });
        toast.present();
    }
}
