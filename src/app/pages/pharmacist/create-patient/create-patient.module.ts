import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {CreatePatientPageRoutingModule} from './create-patient-routing.module';

import {CreatePatientPage} from './create-patient.page';
import {IonFormErrorMessagesModule} from 'ion-form-error-messages';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        CreatePatientPageRoutingModule,
        IonFormErrorMessagesModule
    ],
    declarations: [CreatePatientPage]
})
export class CreatePatientPageModule {
}
