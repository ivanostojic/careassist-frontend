import {Component, OnInit} from '@angular/core';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {ActivatedRoute, Router} from '@angular/router';
import {DrugService} from '../../../services/drug.service';
import {AuthService} from '../../../services/auth.service';
import {LoadingController} from '@ionic/angular';
import {Geolocation} from '@ionic-native/geolocation/ngx';


@Component({
    selector: 'app-available-drugs',
    templateUrl: './available-drugs.page.html',
    styleUrls: ['./available-drugs.page.scss'],
})
export class AvailableDrugsPage implements OnInit {
    public nearestPharmacies;
    public userLatitude;
    public userLongitude;
    public routeDestination;

    constructor(
        private router: Router,
        private drugService: DrugService,
        private geolocation: Geolocation,
        public route: ActivatedRoute,
        private auth: AuthService,
        public loadingController: LoadingController) {
            if (auth.getUserRole() === 'caretaker') {
                this.routeDestination = '/caretaker-tabs/';
            }
            if (auth.getUserRole() === 'pharmacist') {
                this.routeDestination = '/pharmacist-tabs/';
            }
    }

    ngOnInit() {
        this.getData();
    }

    getUserLocation() {
        return this.geolocation.getCurrentPosition({timeout: 30000, enableHighAccuracy: true});
    }

    async getLoadingIndicator() {
        const loading = await this.loadingController.create({
            message: 'Please wait...'
        });
        loading.present();
        return loading;
    }

    async getData() {
        const loading = await this.getLoadingIndicator();
        const drugId = this.route.snapshot.paramMap.get('drugId');
        const coords = (await this.getUserLocation()).coords;
        const latitude = coords.latitude;
        const longitude = coords.longitude;
        this.drugService.getDrugFromNearestPharmacies(latitude, longitude, drugId).subscribe(res => {
            this.nearestPharmacies = res;
            loading.dismiss();
        });
    }

}
