import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AvailableDrugsPageRoutingModule } from './available-drugs-routing.module';

import { AvailableDrugsPage } from './available-drugs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AvailableDrugsPageRoutingModule
  ],
  declarations: [AvailableDrugsPage]
})
export class AvailableDrugsPageModule {}
