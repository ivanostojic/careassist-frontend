import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AvailableDrugsPage } from './available-drugs.page';

const routes: Routes = [
  {
    path: '',
    component: AvailableDrugsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AvailableDrugsPageRoutingModule {}
