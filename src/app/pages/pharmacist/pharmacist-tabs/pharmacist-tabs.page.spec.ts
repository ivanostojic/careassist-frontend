import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PharmacistTabsPage } from './pharmacist-tabs.page';

describe('PharmacistTabsPage', () => {
  let component: PharmacistTabsPage;
  let fixture: ComponentFixture<PharmacistTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PharmacistTabsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PharmacistTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
