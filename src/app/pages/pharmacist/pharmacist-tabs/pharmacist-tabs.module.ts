import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PharmacistTabsPageRoutingModule } from './pharmacist-tabs-routing.module';

import { PharmacistTabsPage } from './pharmacist-tabs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PharmacistTabsPageRoutingModule
  ],
  declarations: [PharmacistTabsPage]
})
export class PharmacistTabsPageModule {}
