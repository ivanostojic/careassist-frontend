import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {PharmacistTabsPage} from './pharmacist-tabs.page';
import {AuthGuard} from '../../../guards/auth.guard';


const routes: Routes = [
    {
        path: '',
        component: PharmacistTabsPage,
        children: [
            {
                path: 'my-pharmacy',
                loadChildren: () => import('../my-pharmacy/my-pharmacy.module').then(m => m.MyPharmacyPageModule),
                data: { foo: 'parent data' }
            },
            {
                path: 'drugs/filtered-drugs',
                loadChildren: () => import('../filtered-drugs/filtered-drugs.module').then(m => m.FilteredDrugsPageModule)
            },
            {
                path: 'drugs/filtered-drugs/:id',
                loadChildren: () => import('../drug/drug.module').then(m => m.DrugPageModule)
            },
            {
                path: 'patients/create',
                loadChildren: () => import('../create-patient/create-patient.module').then(m => m.CreatePatientPageModule),
            },
            {
                path: 'patients/:id',
                loadChildren: () => import('../patient/patient.module').then(m => m.PatientPageModule)
            },
            {
                path: 'drugs',
                loadChildren: () => import('../drugs/drugs.module').then(m => m.DrugsPageModule)
            },
            {
                path: 'patients',
                loadChildren: () => import('../patients/patients.module').then(m => m.PatientsPageModule)
            },
            {
                path: 'pharmacies/:pharmacyId',
                loadChildren: () => import('../my-pharmacy/my-pharmacy.module').then(m => m.MyPharmacyPageModule)
            },
        ]
    },
    // {
    //     path: 'patients/create',
    //     loadChildren: () => import('../create-patient/create-patient.module').then(m => m.CreatePatientPageModule),
    //     pathMatch: 'full'
    // }
];


// const routes: Routes = [
//   {
//     path: '',
//     component: PharmacistTabsPage
//   }
// ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PharmacistTabsPageRoutingModule {
}
