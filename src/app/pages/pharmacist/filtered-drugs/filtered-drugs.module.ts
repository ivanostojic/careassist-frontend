import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FilteredDrugsPageRoutingModule } from './filtered-drugs-routing.module';

import { FilteredDrugsPage } from './filtered-drugs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FilteredDrugsPageRoutingModule
  ],
  declarations: [FilteredDrugsPage]
})
export class FilteredDrugsPageModule {}
