import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FilteredDrugsPage } from './filtered-drugs.page';

const routes: Routes = [
  {
    path: '',
    component: FilteredDrugsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FilteredDrugsPageRoutingModule {}
