import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FilteredDrugsPage } from './filtered-drugs.page';

describe('FilteredDrugsPage', () => {
  let component: FilteredDrugsPage;
  let fixture: ComponentFixture<FilteredDrugsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilteredDrugsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FilteredDrugsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
