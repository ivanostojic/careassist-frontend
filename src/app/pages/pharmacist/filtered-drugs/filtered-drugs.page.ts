import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-filtered-drugs',
    templateUrl: './filtered-drugs.page.html',
    styleUrls: ['./filtered-drugs.page.scss'],
})
export class FilteredDrugsPage implements OnInit {
    data;
    constructor(private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.data = this.router.getCurrentNavigation().extras.state;
                console.log(this.data);
            }
        });

    }
}
