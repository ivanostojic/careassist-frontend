import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {IonInfiniteScroll} from '@ionic/angular';
import {DrugService} from '../../../services/drug.service';
import {environment} from '../../../../environments/environment';
import {BehaviorSubject, Observable, from, of} from 'rxjs';
import {LoadingController} from '@ionic/angular';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-drug',
    templateUrl: './drug.page.html',
    styleUrls: ['./drug.page.scss'],
})
export class DrugPage implements OnInit {
    @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;
    url = environment.url;
    theDrug;
    stock;
    stockValue;

    constructor(
        private iab: InAppBrowser,
        private router: Router,
        private drug: DrugService,
        public route: ActivatedRoute,
        private auth: AuthService,
        public loadingController: LoadingController
    ) {

    }

    ngOnInit() {
        this.getData();
    }

    async getData() {
        const loading = await this.getLoadingIndicator();
        const id = this.route.snapshot.paramMap.get('id');
        this.drug.getADrug(id).subscribe(res => {
            this.theDrug = res;
            this.theDrug.pharmacies[0] ? this.stock = this.theDrug.pharmacies[0].PharmacyDrug.stock : this.stock = null;
            loading.dismiss();
        });
    }

    async getLoadingIndicator() {
        const loading = await this.loadingController.create({
            message: 'Please wait...'
        });
        loading.present();
        return loading;
    }

    hasPharmacy() {
        console.log(this.auth.getUser().pharmacyId !== null);
        return this.auth.getUser().pharmacyId !== null;
    }

    openPdfSystem() {
        this.iab.create('https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf', '_system');
    }

    openPdfSystemProduction() {
        this.iab.create(`${this.url}/api/documents/${this.theDrug.drugPdf}`, '_system');
    }

    modifyStock(addStock) {
        const id = this.route.snapshot.paramMap.get('id');
        const stockValue = this.stockValue;

        if (this.stock === null) {
            if (!addStock) {
                return;
            }
            const formAddDrug = {
                stockValue
            };
            return this.drug.addDrugToPharmacy(id, formAddDrug).subscribe(res => {
                this.getData();
            });
        }
        if (!addStock && stockValue > this.stock) {
            return;
        }
        const form = {
            addStock,
            stockValue
        };
        this.drug.modifyStock(id, form).subscribe(res => {
            this.getData();
        });
    }
}
