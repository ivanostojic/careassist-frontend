import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyPharmacyPage } from './my-pharmacy.page';

const routes: Routes = [
  {
    path: '',
    component: MyPharmacyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyPharmacyPageRoutingModule {}
