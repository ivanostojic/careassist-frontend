import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyPharmacyPageRoutingModule } from './my-pharmacy-routing.module';

import { MyPharmacyPage } from './my-pharmacy.page';

import {AgmCoreModule} from '@agm/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MyPharmacyPageRoutingModule,
        AgmCoreModule
    ],
  declarations: [MyPharmacyPage]
})
export class MyPharmacyPageModule {}
