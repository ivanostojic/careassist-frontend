import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DrugService} from '../../../services/drug.service';
import {AuthService} from '../../../services/auth.service';
import {Location} from '@angular/common';

declare const google: any;

@Component({
    selector: 'app-my-pharmacy',
    templateUrl: './my-pharmacy.page.html',
    styleUrls: ['./my-pharmacy.page.scss'],
})
export class MyPharmacyPage implements OnInit {
    public pharmacy;
    public zoom = 14;
    public pharmacyId;

    constructor(
        private router: Router,
        private drug: DrugService,
        public route: ActivatedRoute,
        private auth: AuthService,
        private location: Location
    ) {

    }

    ngOnInit() {
        this.route.url.subscribe(() => {
            const pharmacyId = this.route.snapshot.paramMap.get('pharmacyId');
            this.pharmacyId = pharmacyId ? pharmacyId : this.auth.getUser().pharmacyId;
        });

        this.getData();
    }

    async getData() {
        this.drug.getPharmacy(this.pharmacyId).subscribe(res => {
            this.pharmacy = res;
        });
    }

    goBack() {
        this.location.back();
    }
}
