import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyPharmacyPage } from './my-pharmacy.page';

describe('MyPharmacyPage', () => {
  let component: MyPharmacyPage;
  let fixture: ComponentFixture<MyPharmacyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPharmacyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyPharmacyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
