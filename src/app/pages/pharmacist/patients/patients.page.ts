import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {PatientService} from '../../../services/patient.service';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-patients',
    templateUrl: './patients.page.html',
    styleUrls: ['./patients.page.scss'],
})
export class PatientsPage implements OnInit {
    data;
    patients;
    allPatients;

    constructor(
        private router: Router,
        private patient: PatientService,
        public route: ActivatedRoute,
        public auth: AuthService,
        public loadingController: LoadingController) {
    }

    ngOnInit() {
        this.getData();
    }

    async getData() {
        // const loading = await this.getLoadingIndicator();
        // const id = this.route.snapshot.paramMap.get('id');
        await this.patient.getPatients().subscribe(res => {
            this.patients = res;
            this.allPatients = res;
            console.log(this.patients);
            // loading.dismiss();
            // console.log(this.theDrug);
        });
    }

    isPharmacist() {
        return this.auth.getUserRole() === 'pharmacist';
    }

    initializeItems() {
        this.patients = this.allPatients;
    }

    // getItems(event) {
    //     console.log(this.patients)
    //     const val = event.target.value;
    //     this.patients = this.allPatients.filter(item => {
    //         return item.firstName.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
    //             item.lastName.toLowerCase().indexOf(val.toLowerCase()) > -1;
    //     });
    // }

    getItems(event) {
        const val = event.target.value;
        this.patients = this.allPatients.filter(item => {
            return (item.firstName.toLowerCase() + ' ' + item.lastName.toLowerCase()).indexOf(val.toLowerCase()) > -1 ||
                item.DNI.indexOf(val) > -1;
        });
    }
}
