import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientPageRoutingModule } from './patient-routing.module';

import { PatientPage } from './patient.page';
import {CaretakersComponent} from '../../../components/caretakers/caretakers.component';
import {AddTreatmentComponent} from '../../../components/add-treatment/add-treatment.component';

@NgModule({
  entryComponents: [CaretakersComponent, AddTreatmentComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientPageRoutingModule
  ],
  declarations: [PatientPage, CaretakersComponent, AddTreatmentComponent]
})
export class PatientPageModule {}
