import {Component, OnInit} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {CaretakersComponent} from '../../../components/caretakers/caretakers.component';
import {ModalController} from '@ionic/angular';
import {DrugService} from '../../../services/drug.service';
import {PatientService} from '../../../services/patient.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {AddTreatmentComponent} from '../../../components/add-treatment/add-treatment.component';


@Component({
    selector: 'app-patient',
    templateUrl: './patient.page.html',
    styleUrls: ['./patient.page.scss'],
})
export class PatientPage implements OnInit {

    public patientData;
    public patientsCaretakers;

    constructor(private patient: PatientService,
                public popoverController: PopoverController,
                private modalCtrl: ModalController,
                private auth: AuthService,
                public route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        this.patient.getPatient(id).subscribe(res => {
            this.patientData = res;
            this.patientsCaretakers = this.patientData.caretakers;
        });
    }

    isPharmacist() {
        return this.auth.getUserRole() === 'pharmacist';
    }

    async displayMod(): Promise<void> {
        const accountListModal = await this.modalCtrl.create({
            component: CaretakersComponent,
            componentProps: {patientId: this.patientData.id},
        });
        return await accountListModal.present();
    }

    async addTreatmentModal(): Promise<void> {
        const accountListModal = await this.modalCtrl.create({
            component: AddTreatmentComponent,
            componentProps: {patientId: this.patientData.id},
        });
        return await accountListModal.present();
    }
}
