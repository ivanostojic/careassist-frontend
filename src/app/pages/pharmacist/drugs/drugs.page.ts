import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DrugService} from '../../../services/drug.service';
import {LoadingController} from '@ionic/angular';

@Component({
    selector: 'app-drugs',
    templateUrl: './drugs.page.html',
    styleUrls: ['./drugs.page.scss'],
})
export class DrugsPage implements OnInit {
    data = {
        genericName: '',
        brandName: '',
        Tags: [],
        year: '',
        DrugClasses: [],
        Shapes: []
    };
    allTags;
    allShapes;
    allDrugClasses;

    constructor(private drug: DrugService,
                private router: Router,
                public loadingController: LoadingController) {
    }

    ngOnInit() {
        this.drug.getTags().subscribe(res => {
            this.allTags = res;
        });
        this.drug.getShapes().subscribe(res => {
            this.allShapes = res;
        });
        this.drug.getDrugClasses().subscribe(res => {
            this.allDrugClasses = res;
        });
    }

    sendForm() {
        this.drug.filterDrugs(this.data).subscribe(res => {
            // console.log(res);
            this.router.navigate([this.router.url + '/filtered-drugs'], {state: res});
        });
    }

    // sendForm() {
    //     this.auth.login(this.credentials).subscribe(async res => {
    //         if (res) {
    //             console.log(res);
    //             this.checkRole();
    //         } else {
    //             const alert = await this.alertCtrl.create({
    //                 header: 'Login Failed',
    //                 message: 'Wrong credentials.',
    //                 buttons: ['OK']
    //             });
    //             await alert.present();
    //         }
    //     });
    // }
}
