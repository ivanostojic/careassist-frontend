import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DrugsPage } from './drugs.page';

const routes: Routes = [
  {
    path: '',
    component: DrugsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DrugsPageRoutingModule {}
