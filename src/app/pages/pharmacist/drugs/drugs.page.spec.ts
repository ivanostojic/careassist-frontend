import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DrugsPage } from './drugs.page';

describe('DrugsPage', () => {
  let component: DrugsPage;
  let fixture: ComponentFixture<DrugsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DrugsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
