import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {DrugsPageRoutingModule} from './drugs-routing.module';

import {DrugsPage} from './drugs.page';
import {RouterModule, Routes} from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{ path: '', component: DrugsPage }])
    ],
    declarations: [DrugsPage]
})
export class DrugsPageModule {
}
