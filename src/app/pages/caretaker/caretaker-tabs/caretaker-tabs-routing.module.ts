import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CaretakerTabsPage } from './caretaker-tabs.page';
import {AuthGuard} from '../../../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: CaretakerTabsPage,
    children: [
      {
        path: '',
        redirectTo: '/caretaker-tabs/drugs',
        pathMatch: 'full'
      },
      {
        path: 'patients',
        loadChildren: () => import('../../pharmacist/patients/patients.module').then(m => m.PatientsPageModule)
      },
      {
        path: 'patients/create',
        loadChildren: () => import('../../pharmacist/create-patient/create-patient.module').then(m => m.CreatePatientPageModule)
      },
      {
        path: 'patients/:id',
        loadChildren: () => import('../../pharmacist/patient/patient.module').then(m => m.PatientPageModule)
      },
      {
        path: 'drugs',
        loadChildren: () => import('../../pharmacist/drugs/drugs.module').then(m => m.DrugsPageModule)
      },
      {
        path: 'drugs/filtered-drugs',
        loadChildren: () => import('../../pharmacist/filtered-drugs/filtered-drugs.module').then(m => m.FilteredDrugsPageModule)
      },
      {
        path: 'pharmacies/:pharmacyId',
        loadChildren: () => import('../../pharmacist/my-pharmacy/my-pharmacy.module').then(m => m.MyPharmacyPageModule)
      },
      {
        path: 'drugs/filtered-drugs/:id',
        loadChildren: () => import('../../pharmacist/drug/drug.module').then(m => m.DrugPageModule),
        canActivate: [AuthGuard],
        data: {
          role: 'caretaker'
        }
      },
      {
        path: 'treatments',
        loadChildren: () => import('../../caretaker/treatments/treatments.module').then(m => m.TreatmentsPageModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CaretakerTabsPageRoutingModule {}
