import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CaretakerTabsPageRoutingModule } from './caretaker-tabs-routing.module';

import { CaretakerTabsPage } from './caretaker-tabs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CaretakerTabsPageRoutingModule
  ],
  declarations: [CaretakerTabsPage]
})
export class CaretakerTabsPageModule {}
