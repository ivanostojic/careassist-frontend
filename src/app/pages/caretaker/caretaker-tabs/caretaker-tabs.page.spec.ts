import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CaretakerTabsPage } from './caretaker-tabs.page';

describe('CaretakerTabsPage', () => {
  let component: CaretakerTabsPage;
  let fixture: ComponentFixture<CaretakerTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaretakerTabsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CaretakerTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
