import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {AlertController} from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
    credentials = {
        email: '',
        password: ''
    };

    constructor(
        private auth: AuthService,
        private router: Router,
        private alertCtrl: AlertController
    ) {
    }

    ngOnInit() {
    }

    checkRole() {
        const userRole = this.auth.getUserRole();
        if (userRole == 'admin') {
            this.router.navigateByUrl('admin-dashboard');
        }
        if (userRole == 'pharmacist') {
            this.router.navigateByUrl('pharmacist-tabs/drugs');
        }
        if (userRole == 'caretaker') {
            this.router.navigateByUrl('caretaker-tabs');
        }
    }

    logout() {
        this.auth.logout();
    }

    login() {
        this.auth.login(this.credentials).subscribe(async res => {
            if (res) {
                console.log(res);
                this.checkRole();
            } else {
                const alert = await this.alertCtrl.create({
                    header: 'Login Failed',
                    message: 'Wrong credentials.',
                    buttons: ['OK']
                });
                await alert.present();
            }
        });
    }

}
