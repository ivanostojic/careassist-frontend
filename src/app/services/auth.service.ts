import {Platform} from '@ionic/angular';
import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {BehaviorSubject, Observable, from, of} from 'rxjs';
import {take, map, switchMap, tap, catchError} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {HelperFunctions} from '../helpers/helper';

const jwtHelper = new JwtHelperService();
const TOKEN_KEY = 'jwt_token';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    url = environment.url;
    public user: Observable<any>;
    private authenticationState = new BehaviorSubject(null);

    constructor(private storage: Storage, private jwtHelper: JwtHelperService, private http: HttpClient, private plt: Platform,
                private router: Router, private helperFunctions: HelperFunctions) {
        this.loadStoredToken();
    }

    loadStoredToken() {
        let platformObs = from(this.plt.ready());
        this.user = platformObs.pipe(
            switchMap(() => {
                return from(this.storage.get(TOKEN_KEY));
            }),
            map(token => {
                if (token) {
                    let decoded = jwtHelper.decodeToken(token);
                    let isExpired = this.jwtHelper.isTokenExpired(token);
                    if (!isExpired) {
                        this.authenticationState.next(decoded);
                        return true;
                    } else {
                        this.storage.remove(TOKEN_KEY);
                        return null;
                    }
                }
            })
        );
    }

    checkToken() {
        this.storage.get(TOKEN_KEY).then(token => {
            if (token) {
                let decoded = this.jwtHelper.decodeToken(token);
                let isExpired = this.jwtHelper.isTokenExpired(token);

                if (!isExpired) {
                    this.user = decoded;
                    console.log(this.user);
                    this.authenticationState.next(this.user);
                } else {
                    this.storage.remove(TOKEN_KEY);
                }
            }
        });
    }

    // loadStoredToken() {
    //     let platformObs = from(this.plt.ready());
    //
    //     this.user = platformObs.pipe(
    //         switchMap(() => {
    //             return from(this.storage.get(TOKEN_KEY));
    //         }),
    //         map(token => {
    //             if (token) {
    //                 let decoded = jwtHelper.decodeToken(token);
    //                 this.authenticationState.next(decoded);
    //                 return true;
    //             } else {
    //                 return null;
    //             }
    //         })
    //     );
    // }

    login(credentials: { email: string, password: string }) {
        return this.http.post(`${this.url}/api/auth/login`, credentials).pipe(
            tap(res => {
                this.storage.set(TOKEN_KEY, res['token']);
                console.log(res['token']);
                this.user = of(true);
                let decoded = jwtHelper.decodeToken(res['token']);
                console.log(decoded);
                this.authenticationState.next(decoded);
            }),
            catchError(e => {
                this.helperFunctions.showAlert(e.error.message);
                // this.showAlert(e.error.message);
                throw new Error(e);
            })
        );
    }

    getToken() {
        return from(this.storage.get(TOKEN_KEY));
    }

    isAuthenticated() {
        return this.authenticationState.value;
    }

    getUser() {
        return this.authenticationState.getValue();
    }

    getUserRole() {
        return this.authenticationState.getValue().role;
    }

    logout() {
        this.storage.remove(TOKEN_KEY).then(() => {
            this.router.navigateByUrl('/');
            this.authenticationState.next(null);
        });
    }
}
