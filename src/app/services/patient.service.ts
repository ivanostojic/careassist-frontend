import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HelperFunctions} from '../helpers/helper';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {take, map, switchMap, tap, catchError} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PatientService {
    url = environment.url;

    constructor(private http: HttpClient, private helperFunctions: HelperFunctions) {
    }

    getPatients() {
        return this.http.get(`${this.url}/api/patients`).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
            })
        );
    }

    getAllCaretakers() {
        return this.http.get(`${this.url}/api/users/caretakers`).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
            })
        );
    }

    storePatient(patient) {
        return this.http.post(`${this.url}/api/patients`, patient).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.name);
            })
        );
    }

    getPatient(id) {
        return this.http.get(`${this.url}/api/patients/${id}`).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.name);
            })
        );
    }

    attachNewCaretaker(patientId, caretakerId) {
        return this.http.post(`${this.url}/api/patients/${patientId}/caretakers`, caretakerId).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.name);
                return false;
            })
        );
    }

    detachCaretaker(patientId, caretakerId) {
        return this.http.put(`${this.url}/api/patients/${patientId}/caretakers`, caretakerId).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.name);
            })
        );
    }
}
