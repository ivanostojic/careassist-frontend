import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HelperFunctions} from '../helpers/helper';
import {HttpClient, HttpParams} from '@angular/common/http';
import {take, map, switchMap, tap, catchError} from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class DrugService {
    url = environment.url;

    constructor(private http: HttpClient, private helperFunctions: HelperFunctions) {
    }

    getADrug(id) {
        return this.http.get(`${this.url}/api/drugs/${id}`).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
                throw new Error(e);
            })
        );
    }

    addDrugToPharmacy(drugId, form) {
        return this.http.post(`${this.url}/api/drugs/${drugId}/pharmacy`, form).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
                throw new Error(e);
            })
        );
    }

    modifyStock(drugId, form) {
        return this.http.post(`${this.url}/api/drugs/${drugId}/stock`, form).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
                throw new Error(e);
            })
        );
    }

    getDrugFromNearestPharmacies(latitude = null, longitude = null, drugId = '', pharmacyId = '') {
        let params = new HttpParams();
        params = params.append('latitude', latitude);
        params = params.append('longitude', longitude);
        params = params.append('drugId', drugId);
        params = params.append('pharmacyId', pharmacyId);
        return this.http.get(`${this.url}/api/drugs/pharmacy_drugs`, {params}).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
            })
        );
    }

    getPharmacy(pharmacyId) {
        return this.http.get(`${this.url}/api/pharmacies/${pharmacyId}`).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
            })
        );
    }

    getTags() {
        return this.http.get(`${this.url}/api/drugs/tags`).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
                throw new Error(e);
            })
        );
    }

    getShapes() {
        return this.http.get(`${this.url}/api/drugs/shapes`).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
                throw new Error(e);
            })
        );
    }

    getDrugClasses() {
        return this.http.get(`${this.url}/api/drugs/drug-classes`).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
                throw new Error(e);
            })
        );
    }

    filterDrugs(form) {
        console.log(form);
        return this.http.post(`${this.url}/api/drugs/search-drugs`, form).pipe(
            catchError(async e => {
                this.helperFunctions.showAlert(e.error.message);
                throw new Error(e);
            })
        );
    }
}
