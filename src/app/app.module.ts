import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {Storage, IonicStorageModule} from '@ionic/storage';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './helpers/AuthInterceptor';
import {JwtModule, JWT_OPTIONS, JwtHelperService} from '@auth0/angular-jwt';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {from, Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {IonFormErrorMessagesModule} from 'ion-form-error-messages';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {AgmCoreModule} from '@agm/core';

export function jwtOptionsFactory(storage) {
    return {
        tokenGetter: () => {
            return storage.get('jwt_token');
        },
        whitelistedDomains: [environment.urn]
        // throwNoTokenError: true
    };
}

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
        IonicStorageModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: ''
        }),
        HttpClientModule,
        IonFormErrorMessagesModule,
        JwtModule.forRoot({
            jwtOptionsProvider: {
                provide: JWT_OPTIONS,
                useFactory: jwtOptionsFactory,
                deps: [Storage]
            }
        })],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
        // {provide: JWT_OPTIONS, useValue: JWT_OPTIONS},
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        JwtHelperService,
        InAppBrowser,
        Geolocation,
        LocalNotifications
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
