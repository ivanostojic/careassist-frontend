import {Component, Input, OnInit} from '@angular/core';
import {ModalController, ToastController} from '@ionic/angular';
import {PatientService} from '../../services/patient.service';
import {HelperFunctions} from '../../helpers/helper';

@Component({
    selector: 'app-caretakers',
    templateUrl: './caretakers.component.html',
    styleUrls: ['./caretakers.component.scss'],
})
export class CaretakersComponent implements OnInit {
    @Input() public patientId;
    public patientCaretakers;
    public allCaretakers;
    public newCaretakerId;

    constructor(private modalCtrl: ModalController,
                private patient: PatientService,
                public toastController: ToastController,
                public errorModal: HelperFunctions) {
    }

    ngOnInit() {
        // this.patient.getPatient(this.patientId).subscribe((res: any) => {
        //     this.patientCaretakers = res.caretakers;
        // });
        this.getPatientsCaretakers();

        this.patient.getAllCaretakers().subscribe(res => {
            this.allCaretakers = res;
        });
    }

    dismiss(): void {
        this.modalCtrl.dismiss();
    }

    getPatientsCaretakers() {
        this.patient.getPatient(this.patientId).subscribe((res: any) => {
            this.patientCaretakers = res.caretakers;
        });
    }

    submitNewCaretaker() {
        if (!this.newCaretakerId) {
            return this.errorModal.showAlert('You didn \'t specify any caretaker');
        }
        this.patient.attachNewCaretaker(this.patientId, {caretakerId: this.newCaretakerId}).subscribe(success => {
            if (success) {
                this.presentToast('You have successfully added a new caretaker.');
                this.getPatientsCaretakers();
            }
        });
    }

    detachCaretaker(caretakerId) {
        this.patient.detachCaretaker(this.patientId, {caretakerId}).subscribe(res => {
            this.presentToast('You have successfully removed a caretaker from this patient.');
            this.getPatientsCaretakers();
        });
    }

    async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    }
}
