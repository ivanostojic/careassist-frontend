import {Component, Input, OnInit} from '@angular/core';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {AlertController, ModalController, Platform} from '@ionic/angular';

@Component({
    selector: 'app-add-treatment',
    templateUrl: './add-treatment.component.html',
    styleUrls: ['./add-treatment.component.scss'],
})
export class AddTreatmentComponent implements OnInit {
    @Input() public patientId;
    public data = {
        notificationDate: null,
        header: '',
        message: ''
    };

    constructor(private localNotifications: LocalNotifications, private plt: Platform,
                public alertCtrl: AlertController, private modalCtrl: ModalController) {
        this.plt.ready().then(() => {
            this.localNotifications.on('trigger').subscribe(res => {
                console.log(res);
                const dateTime = new Date(res.trigger.at);
                this.showAlert(res.title, `${dateTime.toLocaleDateString()} ${dateTime.toLocaleTimeString()}`, res.text);
            });
        });
    }

    ngOnInit() {
    }

    showAlert(header, subHeader, message) {
        const alert = this.alertCtrl.create({
            header,
            subHeader,
            message,
            buttons: ['OK']
        });
        alert.then(alr => alr.present());
    }

    dismiss(): void {
        this.modalCtrl.dismiss();
    }

    sendForm() {
        console.log(this.data);
        this.localNotifications.schedule({
            title: this.data.header,
            text: this.data.message,
            trigger: {at: new Date(this.data.notificationDate)}
        });
    }
}
